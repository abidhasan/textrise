import Show from '../models/Show';

const sampleShowOptions = {
  name: 'My first!',
  dimensions: {x: 500, y: 600},
  id: 0,
  // options: {},
};

export const sampleShow = [
  new Show(sampleShowOptions),
];

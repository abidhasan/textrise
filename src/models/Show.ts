import Image from '../models/Image';
import Slide from '../models/Slide';
import * as constants from '../data/constants';
import { Size } from './Types';

// interface ShowOptions {}

export interface SlideInterface {
  backgroundImage: string;
  icon: string;
  displayDuration: number;
  updateFrequency: number;
}

interface ShowInterface {
  id: number;
  name: string;
  // options: ShowOptions;
  dimensions: Size;
  image?: string;
}

export default class Show {
  public version: string;
  public id: number;
  public name: string;
  // public options: ShowOptions;
  public dimensions: Size;
  public image: Image | undefined;
  public slides: Slide[];
  public slideId: number;

  constructor(showOptions: ShowInterface) {
    // Basics
    this.version = constants.APP_VERSION.toString();
    this.id = showOptions.id;

    // User set options
    this.name = showOptions.name;
    // this.options = {};
    this.dimensions = { ...showOptions.dimensions };
    if (showOptions.image) {
      this.image = new Image({img: showOptions.image});
    } else {
      this.image = undefined;
    }

    // Slides
    this.slides = [];
    this.slideId = 0;
  }

  public getNewSlideId() {
    return this.slideId++;
  }

  public addSlide(options: SlideInterface) {
    // background_img, icon, options, showDuration, updateFrequency
    const slideOptions = {
      id: this.getNewSlideId(),
      ...options,
    };
    const newSlide = new Slide(slideOptions);
    this.slides.push(newSlide);
    return newSlide;
  }
}

import Vue from 'vue';

export interface ImageInterface {
  img: string;
  alt?: string;
}

export default class Image {
  public img: string;
  public alt: string;

  constructor(options: ImageInterface) {
    const { img, alt } = options;
    // TO--DO: determine if img is base64, URL or local path.
    // In any case, convert to base64 and store.
    this.img = img;
    this.alt = alt || '';
  }

  public get src() {
    return this.img || 'https://www.flaticon.com/premium-icon/icons/svg/450/450016.svg';
  }

  // public render() {
  //   const url = 'https://www.flaticon.com/premium-icon/icons/svg/450/450016.svg';
  //   return {
  //     functional: true,
  //     render(createElement) {
  //       return createElement('img', { attrs: { 'src': url } });
  //     },
  //   }
  //   // Use this component with <component :is="imageObject.render()" />
  // }
}

interface WidgetOptionsInterface {
  type: string;
  text: string;
  width?: number;
  fontSize?: number;
  fontWeight?: number;
  onChange?: (e: any) => void;
  updates?: string;
}

export default class WidgetOptions {
  public type: string;
  public text: string;
  public width?: number;
  public fontSize?: number;
  public fontWeight?: number;
  public onChange?: (e: any) => void;
  public updates?: string;

  constructor(options: WidgetOptionsInterface) {
    // Options go here
    const { type, text } = options;
    this.type = type;
    this.text = text;
  }

  public render() {
    // based on type and text, show a control, bind to onChange handler passed in options
  }
}

import Image from './Image';
import Widget from './Widget';
import { Position, Size } from './Types';

export interface SlideInterface {
  backgroundImage: string;
  icon: string;
  displayDuration: number;
  updateFrequency: number;
  id: number;
}

interface AddWidgetInterface {
  type: string;
  size: Size;
  position: Position;
}

export default class Slide {
  public backgroundImage: Image;
  public icon: Image;
  public options: { displayDuration: number, updateFrequency: number };
  public id: number;
  public layout: Widget[];
  private widgetId: number;

  constructor(options: SlideInterface) {
    const { backgroundImage, icon, displayDuration, updateFrequency, id } = options;
    // Image and icon
    this.backgroundImage = new Image({img: backgroundImage});
    this.icon = new Image({img: icon});

    // Options
    this.options = {
      displayDuration,
      updateFrequency,
    };
    this.id = id;
    this.layout = [];
    this.widgetId = 0;
  }

  public addWidget(options: AddWidgetInterface) {
    // TODO: type should determine which Widget to use! --- position size and type
    const { type } = options;
    const newWidgetOptions = {
      id: this.getNewWidgetId(),
      ...options,
    };
    const newWidget = new Widget(newWidgetOptions);
    this.layout.push(newWidget);
    return newWidget;
  }

  public moveWidget(id: number, position: Position) {
    const existingWidget = this.layout.findIndex((widget) => widget.id === id);
    if (existingWidget === -1) {
      throw Error('Widget not found!');
      return;
    }
    this.layout[existingWidget].position = position;
  }

  public render() {
    // Call update method on Widgets (if its past expiry), then call render on Widget
  }

  private getNewWidgetId() {
    return this.widgetId++;
  }
}

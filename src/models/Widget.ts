import WidgetOptions from './WidgetOptions';
import { Position, Size } from './Types';

const VERSION = 1.0;
const UPDATE_FREQ = 1000;

interface WidgetInterface {
  type: string;
  size: Size;
  position: Position;
  id: number;
}

interface WidgetInternalOptions {
  lastUpdated: number;
  updateFreq: number;
}

export default class Widget {
  public version: string;
  public id: number;
  public position: Position;
  public size: Size;
  public options: WidgetInternalOptions;
  public setOptions: WidgetOptions[];

  constructor(options: WidgetInterface) {
    const { position, size, id } = options;

    // Basics
    this.version = VERSION.toString();
    this.id = id;

    // Layout
    this.position = { ...position };
    this.size = { ...size };

    // Options
    this.options = {
      lastUpdated: Date.now(),
      updateFreq: UPDATE_FREQ,
    };
    this.setOptions = [
      new WidgetOptions({type: 'label', text: 'WIDGET', width: 100}),
    ];
  }

  public update() {
    // Business logic for widget!
  }

  public created() {
    // Called after constructor, and can be asynchronous
  }

  public render() {
    return null;
  }
}

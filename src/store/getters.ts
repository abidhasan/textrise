import { RootState } from './storeTypes';
import Show from '@/models/Show';

export const getters = {
    currentSlideId(state: RootState): number | null {
        const { currentlyEditingSlide } = state.ui;
        return currentlyEditingSlide ? currentlyEditingSlide.id : null;
    },
    getShowById(state: RootState): (showId: number) => Show | null {
        return (showId: number): Show | null => {
            const show = state.shows.find((currentShow: Show) => {
                return currentShow.id === showId;
            });
            return show || null;
        };
    },
};

import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { RootState } from './storeTypes';
import { sampleShow } from '../data/sampleData';
import Show from '../models/Show';
import { actions } from './actions';
import { mutations } from './mutations';
import { getters } from './getters';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  state: {
    shows: [...sampleShow],
    ui: {
      showOptions: false,
      currentlyEditingShow: null,
      currentlyEditingSlide: null,
    },
  },
  getters,
  mutations,
  actions,
};

export default new Vuex.Store<RootState>(store);

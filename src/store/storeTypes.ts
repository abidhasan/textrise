import Show from '../models/Show';
import Slide from '../models/Slide';

export interface RootState {
  shows: Show[];
  ui: {
    showOptions: boolean;
    currentlyEditingShow: Show|null;
    currentlyEditingSlide: Slide|null;
  };
}

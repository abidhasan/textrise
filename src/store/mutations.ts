import { MutationTree } from 'vuex';
import { RootState } from './storeTypes';
import Show from '../models/Show';
import { SlideInterface } from '@/models/Show';

export const mutations: MutationTree<RootState> = {
  ADD_SHOW(state, payload: { show: Show }) {
    state.shows.push(payload.show);
  },
  SHOW_OPTIONS_OVERLAY(state) {
    state.ui.showOptions = true;
  },
  CLOSE_OPTIONS_OVERLAY(state) {
    state.ui.showOptions = false;
  },
  ADD_NEW_SLIDE(state, payload: { currentShow: Show, slideOptions: SlideInterface }) {
    const { currentShow, slideOptions } = payload;
    const newSlide = currentShow.addSlide({...slideOptions});
    state.ui.currentlyEditingSlide = newSlide;
  },
  CURRENTLY_EDITING_SHOW(state, show) {
    state.ui.currentlyEditingShow = show;
  },
  CURRENTLY_EDITING_SLIDE(state, slide) {
    state.ui.currentlyEditingSlide = slide;
  },
  UPDATE_SHOW_NAME(state, title) {
    if (state.ui.currentlyEditingShow) {
      state.ui.currentlyEditingShow.name = title;
    }
  },
};

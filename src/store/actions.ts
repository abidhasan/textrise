import { ActionTree } from 'vuex';
import { RootState } from './storeTypes';
import Slide from '../models/Slide';
import Show from '../models/Show';
import { ADD_SHOW_DEFAULTS, DEFAULT_SLIDE_OPTIONS } from '../defaults';

export const actions: ActionTree<RootState, RootState> = {
  ADD_SHOW({ state, commit }) {
    // Add new show, and show overlay
    const newShowId = state.shows[state.shows.length - 1].id + 1;
    const newShow = new Show({
      id: newShowId,
      name: ADD_SHOW_DEFAULTS.name,
      dimensions: ADD_SHOW_DEFAULTS.dimensions,
    });
    commit('ADD_SHOW', {show: newShow});
    commit('SHOW_OPTIONS_OVERLAY');
    return newShow;
  },
  SHOW_OPTIONS_OVERLAY({ commit }) {
    // Show overlay in UI state
    commit('SHOW_OPTIONS_OVERLAY');
  },
  CLOSE_OPTIONS_OVERLAY({ commit }) {
    // Hide overlay in UI state
    commit('CLOSE_OPTIONS_OVERLAY');
  },
  ADD_NEW_SLIDE({ state, commit }) {
    // Add new slide to current show (get current from state)
    const currentShow = state.ui.currentlyEditingShow;
    const slideOptions = {
      ...DEFAULT_SLIDE_OPTIONS,
    };

    commit('ADD_NEW_SLIDE', { currentShow, slideOptions });
  },
  CURRENTLY_EDITING_SHOW({ state, commit }, showId: number|null) {
    const selectedShow = state.shows.find((show: Show) => {
      return show.id === showId;
    });
    commit('CURRENTLY_EDITING_SHOW', selectedShow || null);
  },
  CURRENTLY_EDITING_SLIDE({ state, commit }, slideId: number) {
    if (!state.ui.currentlyEditingShow) {
      throw new Error(`You are not currently editing anything!`);
    }
    const selectedSlide = state.ui.currentlyEditingShow.slides.find((slide: Slide) => {
      return slide.id === slideId;
    });
    if (!selectedSlide) {
      throw new Error(`Could not find slide with id ${slideId}`);
    }
    commit('CURRENTLY_EDITING_SLIDE', selectedSlide);
  },
  UPDATE_SHOW_NAME({ state, commit }, value: string) {
    if (!state.ui.currentlyEditingShow) {
      throw new Error(`No show is currently being edited`);
    }
    commit('UPDATE_SHOW_NAME', value);
  },
};

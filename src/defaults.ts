export const ADD_SHOW_DEFAULTS = {
  name: 'Untitled Show',
  dimensions: {x: 1280, y: 800},
};

export const DEFAULT_SLIDE_OPTIONS = {
  backgroundImage: '',
  icon: '',
  displayDuration: 10000,
  updateFrequency: 10000,
};

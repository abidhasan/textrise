import Slide from '@/models/Slide';
import Widget from '@/models/Widget';

const slideOptions = {
  backgroundImage: 'url1',
  id: 0,
  icon: 'url2',
  options: {},
  displayDuration: 10000,
  updateFrequency: 1000,
};
const newWidgetOptions = {
  position: {x: 0, y: 0},
  size: {x: 100, y: 100},
  type: 'MYWIDGET',
};

describe('Model for Slide.js', () => {
  let slide: Slide;
  let newWidget: Widget;

  beforeEach(() => {
    slide = new Slide(slideOptions);
    newWidget = slide.addWidget(newWidgetOptions);
  });

  it('Constructor can be called without error', () => {
    expect(slide).toBeDefined();
  });

  it('Adding a widget works', () => {
    expect(slide.layout.length).toBe(1);
  });

  it('Moving a widget works', () => {
    const newPosition = {x: 1000, y: 1000};
    slide.moveWidget(newWidget.id, newPosition);
    expect(slide.layout[0].position).toMatchObject(newPosition);
  });

  it('Moving a non-existent widget throws error', () => {
    const newPosition = {x: 1000, y: 1000};
    expect(() => {
      slide.moveWidget(999, newPosition);
    }).toThrow();
  });
});

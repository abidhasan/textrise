import Widget from '@/models/Widget';
import WidgetOptions from '@/models/WidgetOptions';

describe('Model for Widget.js', () => {
  let widget: Widget;

  beforeEach(() => {
    const options = {
      position: {x: 0, y: 10},
      size: {x: 500, y: 500},
      id: 0,
      type: 'label',
    };
    widget = new Widget(options);
  });

  it('Constructor can be called without error', () => {
    expect(widget).toBeDefined();
  });

  it('Widget provides a version, position and size', () => {
    expect(widget.version).toBeDefined();
  });

  it('Widget setOptions is an arry of WidgetOptions', () => {
    expect(widget.setOptions.every((option) => {
      return option instanceof WidgetOptions;
    })).toBe(true);
  });

  it('Widget has a created() method', () => {
    expect(widget.created).toBeInstanceOf(Function);
  });

  it('Widget has a render() method', () => {
    expect(widget.render).toBeInstanceOf(Function);
  });
});

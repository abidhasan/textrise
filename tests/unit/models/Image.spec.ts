import Image from '@/models/Image';

describe('Model for Image.js', () => {
  it('Constructor can be called without error', () => {
    const image: Image = new Image({img: 'some_url'});
    expect(image).toBeDefined();
  });

  it('Creating an image with an `alt` attribute works', () => {
    const altText = 'alt text';
    const image: Image = new Image({img: 'some_url', alt: altText});
    expect(image.alt).toBe(altText);
  });

  it('Creating an image without an `alt` attribute sets it to be blank', () => {
    const image: Image = new Image({img: 'some_url'});
    expect(image.alt).toBe('');
  });

  it('Creating an image without an `img` attribute sets it to be default', () => {
    const image: Image = new Image({img: ''});
    expect(image.src).not.toBe(undefined);
  });
});

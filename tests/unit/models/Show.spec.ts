// import { shallowMount } from '@vue/test-utils'
import Show from '@/models/Show';
import Slide from '@/models/Slide';

const options = {
  id: 2,
  name: 'Some name',
  dimensions: {
    x: 100,
    y: 100,
  },
  image: 's',
};
const slideOptions = {
  backgroundImage: 'url1',
  id: 0,
  icon: 'url2',
  options: {},
  displayDuration: 10000,
  updateFrequency: 1000,
};

describe('Model for Show.js', () => {
  let newShow: Show;
  let slide: Slide;

  beforeEach(() => {
    newShow = new Show(options);
    slide = newShow.addSlide(slideOptions);
  });

  it('Constructor can be called without error', () => {
    expect(newShow).toBeDefined();
  });

  it('Adding a new slide returns a slide object', () => {
    expect(slide instanceof Slide).toBe(true);
  });

  it('Adding a new slide adds it to Slideshow', () => {
    expect(newShow.slides.length).toBe(1);
  });
});
